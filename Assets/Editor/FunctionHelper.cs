﻿using System.IO;
using UnityEditor;
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace UniDb.Editor
{
    public static class FunctionHelper
    {
        public static void WriteToFile(string filePath, string content)
        {
            using (FileStream fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream, System.Text.Encoding.ASCII))
                {
                    streamWriter.WriteLine(content);
                }
            }
        }
        
        /// <summary>
        /// get content of template file by name
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns>return data content and path of template (string, string) = (data, path)</returns>
        public static (string, string) GetTemplateByName(string templateName)
        {
            var templateAssets = AssetDatabase.FindAssets(templateName);
            if (templateAssets.Length == 0)
            {
                return ("", "");
            }

            var templateGuid = templateAssets[0];
            var templateRelativePath = AssetDatabase.GUIDToAssetPath(templateGuid);
            var templateFormat = (AssetDatabase.LoadAssetAtPath(templateRelativePath, typeof(TextAsset)) as TextAsset)?.ToString();
            return (templateFormat, templateRelativePath);
        }
    }
}