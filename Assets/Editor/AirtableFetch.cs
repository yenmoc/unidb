﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Airtable;
using MasterMemory;
using UnityEditor;
using UnityEngine;
using UnityModule.EditorUtility;

namespace UniDb.Editor
{
    public class AirtableFetch : EditorWindow
    {
        #region properties

        /// <summary>
        /// Api key of airtable
        /// </summary>
        [SerializeField] private string airtableApiKey = "";

        /// <summary>
        /// Base key of airtable
        /// </summary>
        [SerializeField] private string airtableBaseKey = "";

        /// <summary>
        /// List of table names which want to download
        /// </summary>
        [SerializeField] private List<string> wantedTableNames = new List<string>();

        /// <summary>
        /// Position of the scroll view.
        /// </summary>
        private Vector2 _scrollPosition;

        /// <summary>
        /// Progress of download and convert action. 100 is "completed".
        /// </summary>
        private float _progress = 100;

        /// <summary>
        /// The message which be shown on progress bar when action is running.
        /// </summary>
        private string _progressMessage = "";

        #endregion

        #region function

#if MESSAGE_PACK && MEMORY_MASTER
        [MenuItem(ConstantBuilder.MENU_ITEM_AIRTABLE_FETCH_ORIGIN_DATA, false, ConstantBuilder.MENU_ITEM_START_PRIORITY + 11)]
        private static void ShowWindow()
        {
            var window = GetWindow(typeof(AirtableFetch));
            window.titleContent = new GUIContent("AirTable Fetch");
            window.minSize = new Vector2(600, 400);
        }
#endif

        private void Initialized()
        {
            _progress = 100;
            _progressMessage = "";
        }

        private void OnGUI()
        {
            Initialized();

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUI.skin.scrollView);
            GUILayout.BeginVertical();
            {
                GUILayout.Label("Settings", EditorStyles.boldLabel);
                if (PlayerPrefs.HasKey(ConstantBuilder.SAVE_API_KEY) && string.IsNullOrEmpty(airtableApiKey))
                {
                    airtableApiKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_API_KEY);
                }

                if (PlayerPrefs.HasKey(ConstantBuilder.SAVE_BASE_KEY) && string.IsNullOrEmpty(airtableBaseKey))
                {
                    airtableBaseKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_BASE_KEY);
                }

                airtableApiKey = EditorGUILayout.TextField("Api key:", airtableApiKey);
                EditorGUILayout.Space(8);
                airtableBaseKey = EditorGUILayout.TextField("Base key:", airtableBaseKey);

                EditorUtil.DrawUiLine(ColorCollection.Orangered);
                GUILayout.Label("Table names", EditorStyles.boldLabel);
                EditorGUILayout.HelpBox("These tables below will be downloaded. Let the list blank (remove all items) if you want to download all table with name is nameof class in database", MessageType.Info);
                var removeId = -1;
                if (PlayerPrefs.HasKey(ConstantBuilder.SAVE_LIST_TABLE_NAME) && wantedTableNames.Count <= 0)
                {
                    wantedTableNames.Clear();
                    var countTable = PlayerPrefs.GetInt(ConstantBuilder.SAVE_LIST_TABLE_NAME);
                    for (int i = 0; i < countTable; i++)
                    {
                        wantedTableNames.Add(PlayerPrefs.GetString(ConstantBuilder.SAVE_LIST_TABLE_NAME + i));
                    }
                }

                for (var i = 0; i < wantedTableNames.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    wantedTableNames[i] = EditorGUILayout.TextField($"Table {i}", wantedTableNames[i]);
                    if (GUILayout.Button("X", EditorStyles.toolbarButton, GUILayout.Width(20)))
                    {
                        removeId = i;
                    }

                    GUILayout.EndHorizontal();
                }

                if (removeId >= 0) wantedTableNames.RemoveAt(removeId);
                GUILayout.Label($"Download {wantedTableNames.Count} tables");

                if (GUILayout.Button("Add table name", GUILayout.Width(130)))
                {
                    wantedTableNames.Add("");
                }

                if (wantedTableNames.Count > 0 && !string.IsNullOrEmpty(airtableApiKey) && !string.IsNullOrEmpty(airtableBaseKey))
                {
                    GUI.backgroundColor = ColorCollection.LawnGreen;
                    if (GUILayout.Button("Fetch"))
                    {
                        _progress = 0;
                        if (!PlayerPrefs.HasKey(ConstantBuilder.SAVE_SCRIPT_PATH_KEY))
                        {
                            EditorUtility.DisplayDialog("Missing Script Path", "You must select path folder scripts in menu setting via [Database/Settings]", "Ok");
                        }
                        else
                        {
                            GenerateDatabase(PlayerPrefs.GetString(ConstantBuilder.SAVE_SCRIPT_PATH_KEY));
                        }

                        PlayerPrefs.SetString(ConstantBuilder.SAVE_API_KEY, airtableApiKey);
                        PlayerPrefs.SetString(ConstantBuilder.SAVE_BASE_KEY, airtableBaseKey);
                        PlayerPrefs.SetInt(ConstantBuilder.SAVE_LIST_TABLE_NAME, wantedTableNames.Count);
                        for (int i = 0; i < wantedTableNames.Count; i++)
                        {
                            PlayerPrefs.SetString(ConstantBuilder.SAVE_LIST_TABLE_NAME + i, wantedTableNames[i]);
                        }
                    }

                    GUI.backgroundColor = Color.white;
                }

                if (_progress < 100 && _progress > 0)
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100))
                    {
                        _progress = 100;
                        EditorUtility.ClearProgressBar();
                    }
                }
                else
                {
                    EditorUtility.ClearProgressBar();
                }
            }
            try
            {
                GUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            catch (Exception)
            {
                //TODO Sometimes, Unity fire a "InvalidOperationException: Stack empty." bug when Editor want to end a group layout
            }
        }

        private async void GenerateDatabase(string scriptsPath)
        {
            //Validate input
            if (string.IsNullOrEmpty(airtableApiKey))
            {
                Debug.LogError("Api key can not be null!");
                return;
            }

            if (string.IsNullOrEmpty(airtableBaseKey))
            {
                Debug.LogError("Base key can not be null!");
                return;
            }

            if (wantedTableNames.Count <= 0)
            {
                Debug.Log("Enter list table name!");
                return;
            }

            Debug.Log("Downloading...");
            _progressMessage = "Connecting...";
            _progress = 5;
            EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);
            _progressMessage = "Get list of table...";
            EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);

            var type = TypeUtil.GetTypeByName(ConstantBuilder.NAME_MEMORY_DATDABASE_CLASS);
            if (type == null)
            {
                Debug.Log("Please execute generator of MemoryMaster in setting menu [Database/Settings]");
                return;
            }

            var propertyInfos = type.GetProperties();
            if (propertyInfos.Length == 0)
            {
                Debug.Log("No tables exist in the database");
                _progress = 100;
                EditorUtility.ClearProgressBar();
                return;
            }

            _progress = 15;

            var tableRanges = new List<string>(); // contain name of table
            // ReSharper disable once LoopCanBeConvertedToQuery
            foreach (var propertyInfo in propertyInfos)
            {
                var tabbleName = propertyInfo.Name.Replace("Table", "");
                if (wantedTableNames.Contains(tabbleName))
                {
                    tableRanges.Add(tabbleName);
                }
            }

            var (dataCreatorScript, pathDataCreator) = FunctionHelper.GetTemplateByName(ConstantBuilder.DBCREATOR_TEMPLATE_NAME);
            var appends = "";
            foreach (var tableName in tableRanges)
            {
                _progressMessage = $"Processing {tableName}...";
                EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);

                appends += $"{await GenerateDbCreatorFromTemplate(tableName, airtableApiKey, airtableBaseKey)}\n";
                if (wantedTableNames.Count <= 0)
                    _progress += 85f / tableRanges.Count;
                else
                    _progress += 85f / wantedTableNames.Count;
            }

            dataCreatorScript = dataCreatorScript.Replace(ConstantBuilder.DATA_REPLACE, appends);
            dataCreatorScript = Regex.Replace(dataCreatorScript, @"\r\n|\n\r|\r|\n", Environment.NewLine); // Normalize line endings
            var dataCreatorGeneratePath = $"{Application.dataPath}/{scriptsPath}/Editor";
            if (!Directory.Exists(dataCreatorGeneratePath))
            {
                Directory.CreateDirectory(dataCreatorGeneratePath);
            }

            FunctionHelper.WriteToFile($"{dataCreatorGeneratePath}/DBCreator.cs".Replace("/", "\\"), dataCreatorScript);
            _progress = 100;
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log($"<color={ConstantBuilder.GREEN_LOG_COLOR}>Download completed! Please run [Database/Create Database File] to create database file</color>");
        }

        private static async Task<string> GenerateDbCreatorFromTemplate(string tableName, string apiKey, string baseKey)
        {
            var dataResult = ConstantBuilder.DATA_REPLACE_DATABASE_CREATOR;
            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE_BRACKET_OPEN, "\n\t\t\t{\n");
            var dataAppend = "";

            using (var table = new AirtableBase(apiKey, baseKey))
            {
                string offsets = null;

                #region sort

                List<Sort> sorts = null;
                var type = TypeUtil.GetTypeByName(tableName);
                PropertyInfo[] props = null;
                if (type != null)
                {
                    props = type.GetProperties();
                    foreach (var propertyInfo in props)
                    {
                        var hasPrimaryKeyAttribute = Attribute.IsDefined(propertyInfo, typeof(PrimaryKeyAttribute));
                        if (hasPrimaryKeyAttribute)
                        {
                            sorts = new List<Sort> {new Sort {Field = propertyInfo.Name, Direction = SortDirection.Asc}};
                            break;
                        }
                    }
                }

                #endregion

                do
                {
                    var response = await table.ListRecords(tableName, offsets, sort: sorts);
                    offsets = response.offset;
                    if (response.success)
                    {
                        foreach (var responseRecord in response.records)
                        {
                            var dataOneline = ConstantBuilder.DATA_REPLACE_ONE_LINE_DATABASE_CREATOR;
                            var dataProperty = "";
                            dataOneline = dataOneline.Replace(ConstantBuilder.DATA_REPLACE_CLASS_NAME, tableName);
                            var fields = responseRecord.Fields;
                            foreach (var field in fields)
                            {
                                var isArrayType = false;
                                var tempProperty = ConstantBuilder.DATA_REPLACE_PROPERTY_INITIALIZE;
                                tempProperty = tempProperty.Replace(ConstantBuilder.DATA_REPLACE_PROPERTY_NAME, field.Key);

                                if (props != null)
                                {
                                    foreach (var propertyInfo in props)
                                    {
                                        if (field.Key.Equals(propertyInfo.Name) && propertyInfo.PropertyType.IsArray)
                                        {
                                            isArrayType = true;
                                        }
                                    }
                                }

                                var fieldValue = field.Value.ToString();
                                if (!fieldValue.Contains('"') && fieldValue.Contains(',') || isArrayType)
                                {
                                    fieldValue = $@"new [] {{{field.Value}}}";
                                }

                                tempProperty = tempProperty.Replace(ConstantBuilder.DATA_REPLACE_PROPERTY_VALUE, fieldValue);
                                dataProperty += $"{tempProperty}, ";
                            }

                            dataProperty = dataProperty.Remove(dataProperty.Length - 2);
                            dataOneline = dataOneline.Replace(ConstantBuilder.DATA_REPLACE, $"{dataProperty}");
                            dataAppend += $"\t\t\t\t{dataOneline}\n";
                        }
                    }
                    else if (response.airtableApiError != null)
                    {
                        throw new Exception(response.airtableApiError.errorMessage);
                    }
                    else
                    {
                        throw new Exception("Unknown error");
                    }
                } while (offsets != null);
            }

            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE, dataAppend);
            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE_BRACKET_CLOSE, "\t\t\t});");
            return dataResult;
        }

        #endregion
    }
}