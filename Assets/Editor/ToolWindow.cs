﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using System.Collections.Generic;
using System.IO;
using UniDb.Editor;
using UnityEditor;
using UnityEngine;
using UnityModule.EditorUtility;
using UnityModule.Utility;

public class ToolsWindow : EditorWindow
{
#if !UNIDB_INITIALIZED
    private bool _isCreateInitialized;
#endif

    public void OnGUI()
    {
        var style = new GUIStyle {padding = new RectOffset(10, 10, 8, 8)};

        #region startup

#if !UNIDB_INITIALIZED
        if (!_isCreateInitialized)
        {
            EditorGUILayout.BeginHorizontal(style);
            GUILayout.Label("Generate startup file :", GUILayout.Width(120));
            GUILayout.FlexibleSpace();

            GUI.color = ColorCollection.Orangered;
            GUILayout.Label("[ require run ]", GUILayout.Width(78));
            GUILayout.Label("[ only once ]", GUILayout.Width(78));
            GUI.color = Color.white;

            GUI.backgroundColor = ColorCollection.LawnGreen;
            if (GUILayout.Button("Generate", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
            {
#pragma warning disable 0436
                Builder.CreateStartup();
#pragma warning restore 0436
                _isCreateInitialized = true;
            }

            GUI.backgroundColor = Color.white;
            EditorGUILayout.EndHorizontal();
        }
#endif

        #endregion

        #region memory master

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("MemoryMaster Generate :", GUILayout.Width(180));
        GUILayout.FlexibleSpace();
#if !MEMORY_MASTER
        GUI.color = ColorCollection.Orangered;
        GUILayout.Label("[ require run ]", GUILayout.Width(78));
        GUI.color = Color.white;
#endif
        GUI.backgroundColor = ColorCollection.LawnGreen;
        if (GUILayout.Button("Generate", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
        {
            EditorApplication.ExecuteMenuItem("Window/MemoryMaster/Generate");
        }

        GUI.backgroundColor = Color.white;
        EditorGUILayout.EndHorizontal();

        #endregion

        #region message pack

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("MessagePack Generate :", GUILayout.Width(180));
        GUILayout.FlexibleSpace();
#if !MESSAGE_PACK
        GUI.color = ColorCollection.Orangered;
        GUILayout.Label("[ require run ]", GUILayout.Width(78));
        GUI.color = Color.white;
#endif
        GUI.backgroundColor = ColorCollection.LawnGreen;
        if (GUILayout.Button("Generate", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
        {
            EditorApplication.ExecuteMenuItem("Window/MessagePack/[Generate]");
        }

        GUI.backgroundColor = Color.white;
        EditorGUILayout.EndHorizontal();

        #endregion

        EditorUtil.DrawUiLine(ColorCollection.Orange);

        #region open persistent

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("Open persistent data path :", GUILayout.Width(180));
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Open", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON))) OsFileBrowser.Open(Application.persistentDataPath);
        EditorGUILayout.EndHorizontal();

        #endregion

        #region remove all file in persistent

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("Remove all file in persistent data path :", GUILayout.Width(220));
        GUILayout.FlexibleSpace();
        GUI.backgroundColor = ColorCollection.Orangered;
        if (GUILayout.Button("Clear", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
        {
            if (EditorUtility.DisplayDialog("Clear Persistent Data Path", "Are you sure you wish to clear the persistent data path?\nThis action cannot be reversed.", "Clear", "Cancel"))
            {
                var di = new DirectoryInfo(Application.persistentDataPath);

                foreach (var file in di.GetFiles())
                    file.Delete();
                foreach (var dir in di.GetDirectories())
                    dir.Delete(true);
            }
        }

        GUI.backgroundColor = Color.white;
        EditorGUILayout.EndHorizontal();

        #endregion

        #region remove all playerprefs

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("Remove all PlayerPrefs :", GUILayout.Width(180));
        GUILayout.FlexibleSpace();
        GUI.backgroundColor = ColorCollection.Orangered;
        if (GUILayout.Button("Clear All PlayerPrefs", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
            if (EditorUtility.DisplayDialog("Clear PlayerPrefs", "Are you sure you wish to clear PlayerPrefs?\nThis action cannot be reversed.", "Clear", "Cancel"))
                PlayerPrefs.DeleteAll();
        GUI.backgroundColor = Color.white;
        EditorGUILayout.EndHorizontal();

        #endregion

        #region remove all playerprefs keep database key setting

        EditorGUILayout.BeginHorizontal(style);
        GUILayout.Label("Remove all PlayerPrefs but keep database key setting editor :", GUILayout.Width(420));
        GUILayout.FlexibleSpace();
        GUI.backgroundColor = ColorCollection.Yellow;
        if (GUILayout.Button("Clear PlayerPrefs", GUILayout.Width(ConstantBuilder.WIDTH_BUTTON)))
            if (EditorUtility.DisplayDialog("Clear PlayerPrefs Keep", "Are you sure you wish to clear PlayerPrefs?\nThis action cannot be reversed.", "Clear", "Cancel"))
            {
                var airtableApiKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_API_KEY);
                var airtableBaseKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_BASE_KEY);
                var countTable = PlayerPrefs.GetInt(ConstantBuilder.SAVE_LIST_TABLE_NAME);

                var tables = new List<string>();
                for (int i = 0; i < countTable; i++)
                {
                    tables.Add(PlayerPrefs.GetString(ConstantBuilder.SAVE_LIST_TABLE_NAME + i));
                }

                var spreadSheetKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_SHEET_KEY);
                var countSheet = PlayerPrefs.GetInt(ConstantBuilder.SAVE_LIST_SHEET_NAME);
                var sheets = new List<string>();
                for (int i = 0; i < countSheet; i++)
                {
                    sheets.Add(PlayerPrefs.GetString(ConstantBuilder.SAVE_LIST_SHEET_NAME + i));
                }

                PlayerPrefs.DeleteAll();

                PlayerPrefs.SetString(ConstantBuilder.SAVE_API_KEY, airtableApiKey);
                PlayerPrefs.SetString(ConstantBuilder.SAVE_BASE_KEY, airtableBaseKey);
                PlayerPrefs.SetInt(ConstantBuilder.SAVE_LIST_TABLE_NAME, countTable);
                for (int i = 0; i < countTable; i++)
                {
                    PlayerPrefs.SetString(ConstantBuilder.SAVE_LIST_TABLE_NAME + i, tables[i]);
                }

                PlayerPrefs.SetString(ConstantBuilder.SAVE_SHEET_KEY, spreadSheetKey);
                PlayerPrefs.SetInt(ConstantBuilder.SAVE_LIST_SHEET_NAME, countSheet);
                for (int i = 0; i < countSheet; i++)
                {
                    PlayerPrefs.SetString(ConstantBuilder.SAVE_LIST_SHEET_NAME + i, sheets[i]);
                }
            }

        GUI.backgroundColor = Color.white;
        EditorGUILayout.EndHorizontal();

        #endregion
    }

    [MenuItem("Database/Settings", false, ConstantBuilder.MENU_ITEM_START_PRIORITY)]
    private static void ShowWindow()
    {
        var window = GetWindow(typeof(ToolsWindow));
        window.titleContent = new GUIContent("Settings");
        window.minSize = new Vector2(600, 300);
    }
}

public static class OsFileBrowser
{
    public static bool IsInMacOS => SystemInfo.operatingSystem.IndexOf("Mac OS") != -1;

    public static bool IsInWinOS => SystemInfo.operatingSystem.IndexOf("Windows") != -1;

    public static void OpenInMac(string path)
    {
        var openInsidesOfFolder = false;

        // try mac
        var macPath = path.Replace("\\", "/"); // mac finder doesn't like backward slashes

        if (Directory.Exists(macPath)) // if path requested is a folder, automatically open insides of that folder
        {
            openInsidesOfFolder = true;
        }

        if (!macPath.StartsWith("\""))
        {
            macPath = "\"" + macPath;
        }

        if (!macPath.EndsWith("\""))
        {
            macPath = macPath + "\"";
        }

        var arguments = (openInsidesOfFolder ? "" : "-R ") + macPath;

        try
        {
            System.Diagnostics.Process.Start("open", arguments);
        }
        catch (System.ComponentModel.Win32Exception e)
        {
            // tried to open mac finder in windows
            // just silently skip error
            // we currently have no platform define for the current OS we are in, so we resort to this
            e.HelpLink = ""; // do anything with this variable to silence warning about not using it
        }
    }

    public static void OpenInWin(string path)
    {
        var openInsidesOfFolder = false;

        // try windows
        var winPath = path.Replace("/", "\\"); // windows explorer doesn't like forward slashes

        if (Directory.Exists(winPath)) // if path requested is a folder, automatically open insides of that folder
        {
            openInsidesOfFolder = true;
        }

        try
        {
            System.Diagnostics.Process.Start("explorer.exe", (openInsidesOfFolder ? "/root," : "/select,") + winPath);
        }
        catch (System.ComponentModel.Win32Exception e)
        {
            // tried to open win explorer in mac
            // just silently skip error
            // we currently have no platform define for the current OS we are in, so we resort to this
            e.HelpLink = ""; // do anything with this variable to silence warning about not using it
        }
    }

    public static void Open(string path)
    {
        if (IsInWinOS)
        {
            OpenInWin(path);
        }
        else if (IsInMacOS)
        {
            OpenInMac(path);
        }
        else // couldn't determine OS
        {
            OpenInWin(path);
            OpenInMac(path);
        }
    }
}