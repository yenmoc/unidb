﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using UnityModule.EditorUtility;

namespace UniDb.Editor
{
    public class SpreadSheetFetch : EditorWindow
    {
        #region -- properties ------------------------------------------------

        private const string CLIENT_ID = "121683403714-fpqv8gipdkfivqdsi24olgqrtau94l4v.apps.googleusercontent.com";
        private const string CLIENT_SECRET = "ra-CeiVe7xRR1JT4kgub-VE6";
        private const string APP_NAME = "Data Response";
        private static readonly string[] Scopes = {SheetsService.Scope.SpreadsheetsReadonly};

        /// <summary>
        /// Key of the spreadsheet. Get from url of the spreadsheet.
        /// </summary>
        [SerializeField] private string spreadSheetKey = "";

        /// <summary>
        /// List of sheet names which want to download
        /// </summary>
        [SerializeField] private List<string> wantedSheetNames = new List<string>();

        /// <summary>
        /// Position of the scroll view.
        /// </summary>
        private Vector2 _scrollPosition;

        /// <summary>
        /// Progress of download and convert action. 100 is "completed".
        /// </summary>
        private float _progress = 100;

        /// <summary>
        /// The message which be shown on progress bar when action is running.
        /// </summary>
        private string _progressMessage = "";

        #endregion

        #region -- function ------------------------------------------------

#if MESSAGE_PACK && MEMORY_MASTER
        [MenuItem(ConstantBuilder.MENU_ITEM_FETCH_ORIGIN_DATA, false, ConstantBuilder.MENU_ITEM_START_PRIORITY + 11)]
        private static void ShowWindow()
        {
            var window = GetWindow(typeof(SpreadSheetFetch));
            window.titleContent = new GUIContent("SpreadSheet Fetch");
            window.minSize = new Vector2(600, 400);
        }
#endif

        private void Initialized()
        {
            _progress = 100;
            _progressMessage = "";
            ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
        }

        private void OnGUI()
        {
            Initialized();

            _scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition, GUI.skin.scrollView);
            GUILayout.BeginVertical();
            {
                GUILayout.Label("Settings", EditorStyles.boldLabel);
                if (PlayerPrefs.HasKey(ConstantBuilder.SAVE_SHEET_KEY) && string.IsNullOrEmpty(spreadSheetKey))
                {
                    spreadSheetKey = PlayerPrefs.GetString(ConstantBuilder.SAVE_SHEET_KEY);
                }

                spreadSheetKey = EditorGUILayout.TextField("Spread sheet key:", spreadSheetKey);

                EditorUtil.DrawUiLine(ColorCollection.DarkGreen);
                GUILayout.Label("Sheet names", EditorStyles.boldLabel);
                EditorGUILayout.HelpBox("These sheets below will be downloaded. Let the list blank (remove all items) if you want to download all sheets", MessageType.Info);

                var removeId = -1;
                if (PlayerPrefs.HasKey(ConstantBuilder.SAVE_LIST_SHEET_NAME) && wantedSheetNames.Count <= 0)
                {
                    wantedSheetNames.Clear();
                    var sheetCount = PlayerPrefs.GetInt(ConstantBuilder.SAVE_LIST_SHEET_NAME);
                    for (int i = 0; i < sheetCount; i++)
                    {
                        wantedSheetNames.Add(PlayerPrefs.GetString(ConstantBuilder.SAVE_LIST_SHEET_NAME + i));
                    }
                }

                for (var i = 0; i < wantedSheetNames.Count; i++)
                {
                    GUILayout.BeginHorizontal();
                    wantedSheetNames[i] = EditorGUILayout.TextField($"Sheet {i}", wantedSheetNames[i]);
                    if (GUILayout.Button("X", EditorStyles.toolbarButton, GUILayout.Width(20)))
                    {
                        removeId = i;
                    }

                    GUILayout.EndHorizontal();
                }

                if (removeId >= 0) wantedSheetNames.RemoveAt(removeId);
                GUILayout.Label(wantedSheetNames.Count <= 0 ? "Download all sheets" : $"Download {wantedSheetNames.Count} sheets");

                if (GUILayout.Button("Add sheet name", GUILayout.Width(130)))
                {
                    wantedSheetNames.Add("");
                }

                if (wantedSheetNames.Count > 0 && !string.IsNullOrEmpty(spreadSheetKey))
                {
                    GUI.backgroundColor = ColorCollection.LawnGreen;
                    if (GUILayout.Button("Fetch"))
                    {
                        _progress = 0;
                        if (!PlayerPrefs.HasKey(ConstantBuilder.SAVE_SCRIPT_PATH_KEY))
                        {
                            EditorUtility.DisplayDialog("Missing Script Path", "You must select path folder scripts in menu setting via [Database/Settings]", "Ok");
                        }
                        else
                        {
                            GenerateDatabase(PlayerPrefs.GetString(ConstantBuilder.SAVE_SCRIPT_PATH_KEY));
                        }

                        PlayerPrefs.SetString(ConstantBuilder.SAVE_SHEET_KEY, spreadSheetKey);
                        PlayerPrefs.SetInt(ConstantBuilder.SAVE_LIST_SHEET_NAME, wantedSheetNames.Count);
                        for (int i = 0; i < wantedSheetNames.Count; i++)
                        {
                            PlayerPrefs.SetString(ConstantBuilder.SAVE_LIST_SHEET_NAME + i, wantedSheetNames[i]);
                        }
                    }

                    GUI.backgroundColor = UnityEngine.Color.white;
                }

                if ((_progress < 100) && (_progress > 0))
                {
                    if (EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100))
                    {
                        _progress = 100;
                        EditorUtility.ClearProgressBar();
                    }
                }
                else
                {
                    EditorUtility.ClearProgressBar();
                }
            }
            try
            {
                GUILayout.EndVertical();
                EditorGUILayout.EndScrollView();
            }
            catch (Exception)
            {
                //Sometimes, Unity fire a "InvalidOperationException: Stack empty." bug when Editor want to end a group layout
            }
        }

        private void GenerateDatabase(string scriptsPath)
        {
            //Validate input
            if (string.IsNullOrEmpty(spreadSheetKey))
            {
                Debug.LogError("SpreadSheetKey can not be null!");
                return;
            }

            Debug.Log("Downloading with sheet key: " + spreadSheetKey);

            //Authenticate
            _progressMessage = "Authenticating...";
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = GetCredential(),
                ApplicationName = APP_NAME,
            });

            _progress = 5;
            EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);
            _progressMessage = "Get list of spreadsheets...";
            EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);

            var spreadSheetData = service.Spreadsheets.Get(spreadSheetKey).Execute();
            var sheets = spreadSheetData.Sheets;

            if ((sheets == null) || (sheets.Count <= 0))
            {
                Debug.LogError("Not found any data!");
                _progress = 100;
                EditorUtility.ClearProgressBar();
                return;
            }

            _progress = 15;

            //For each sheet in received data, check the sheet name. If that sheet is the wanted sheet, add it into the ranges.
            var ranges = new List<string>();
            foreach (var sheet in sheets)
            {
                if ((wantedSheetNames.Count <= 0) || (wantedSheetNames.Contains(sheet.Properties.Title)))
                {
                    ranges.Add(sheet.Properties.Title);
                }
            }

            var request = service.Spreadsheets.Values.BatchGet(spreadSheetKey);
            request.Ranges = ranges;
            var response = request.Execute();

            var (dataCreatorScript, pathDataCreator) = FunctionHelper.GetTemplateByName(ConstantBuilder.DBCREATOR_TEMPLATE_NAME);
            var appends = "";
            //For each wanted sheet
            foreach (var valueRange in response.ValueRanges)
            {
                var sheetname = valueRange.Range.Split('!')[0];
                _progressMessage = $"Processing {sheetname}...";
                EditorUtility.DisplayCancelableProgressBar("Processing", _progressMessage, _progress / 100);

                appends += $"{GenerateDbCreatorFromTemplate(sheetname, valueRange)}\n";
                if (wantedSheetNames.Count <= 0)
                    _progress += 85f / (response.ValueRanges.Count);
                else
                    _progress += 85f / wantedSheetNames.Count;
            }

            dataCreatorScript = dataCreatorScript.Replace(ConstantBuilder.DATA_REPLACE, appends);
            dataCreatorScript = Regex.Replace(dataCreatorScript, @"\r\n|\n\r|\r|\n", Environment.NewLine); // Normalize line endings
            var dataCreatorGeneratePath = $"{Application.dataPath}/{scriptsPath}/Editor";
            if (!Directory.Exists(dataCreatorGeneratePath))
            {
                Directory.CreateDirectory(dataCreatorGeneratePath);
            }

            FunctionHelper.WriteToFile($"{dataCreatorGeneratePath}/DBCreator.cs".Replace("/", "\\"), dataCreatorScript);
            _progress = 100;
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log($"<color={ConstantBuilder.GREEN_LOG_COLOR}>Download completed! Please run [Database/Create Database File] to create database file</color>");
        }

        private static string GenerateDbCreatorFromTemplate(string fileName, ValueRange valueRange)
        {
            //Get properties's name, data type and sheet data
            IDictionary<int, string> propertyNames = new Dictionary<int, string>(); //Dictionary of (column index, property name of that column)
            IDictionary<int, Dictionary<int, string>> values = new Dictionary<int, Dictionary<int, string>>(); //Dictionary of (row index, dictionary of (column index, value in cell))
            var rowIndex = 0;
            foreach (var row in valueRange.Values)
            {
                var columnIndex = 0;
                foreach (string cellValue in row)
                {
                    var value = cellValue;
                    if (rowIndex == 0)
                    {
                        //This row is properties's name row
                        propertyNames.Add(columnIndex, value);
                    }
                    else
                    {
                        //Data rows
                        //Because first row is name row so we will minus 1 from rowIndex to make data index start from 0
                        if (!values.ContainsKey(rowIndex - 1))
                        {
                            values.Add(rowIndex - 1, new Dictionary<int, string>());
                        }

                        values[rowIndex - 1].Add(columnIndex, value);
                    }

                    columnIndex++;
                }

                rowIndex++;
            }

            //Create list of Dictionaries (property name, value). Each dictionary represent for a object in a row of sheet.
            var dataResult = ConstantBuilder.DATA_REPLACE_DATABASE_CREATOR;
            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE_BRACKET_OPEN, "\n\t\t\t{\n");
            var dataAppend = "";
            foreach (var rowId in values.Keys)
            {
                var dataOneline = ConstantBuilder.DATA_REPLACE_ONE_LINE_DATABASE_CREATOR;
                var dataProperty = "";
                dataOneline = dataOneline.Replace(ConstantBuilder.DATA_REPLACE_CLASS_NAME, fileName);
                foreach (var columnId in propertyNames.Keys)
                {
                    if (!values[rowId].ContainsKey(columnId))
                    {
                        values[rowId].Add(columnId, "");
                    }

                    var tempProperty = ConstantBuilder.DATA_REPLACE_PROPERTY_INITIALIZE;
                    tempProperty = tempProperty.Replace(ConstantBuilder.DATA_REPLACE_PROPERTY_NAME, propertyNames[columnId]);
                    tempProperty = tempProperty.Replace(ConstantBuilder.DATA_REPLACE_PROPERTY_VALUE, values[rowId][columnId]);
                    dataProperty += $"{tempProperty}, ";
                }

                dataProperty = dataProperty.Remove(dataProperty.Length - 2);
                dataOneline = dataOneline.Replace(ConstantBuilder.DATA_REPLACE, $"{dataProperty}");
                dataAppend += $"\t\t\t\t{dataOneline}\n";
            }

            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE, dataAppend);
            dataResult = dataResult.Replace(ConstantBuilder.DATA_REPLACE_BRACKET_CLOSE, "\t\t\t});");
            return dataResult;
        }

        #region -- credential ------------------------------------------------

        private UserCredential GetCredential()
        {
            var ms = MonoScript.FromScriptableObject(this);
            var scriptFilePath = AssetDatabase.GetAssetPath(ms);
            var fi = new FileInfo(scriptFilePath);
            var scriptFolder = fi.Directory?.ToString();
            var unused = scriptFolder?.Replace('\\', '/');
            Debug.Log("Save Credential to: " + scriptFolder);

            UserCredential credential = null;
            var clientSecrets = new ClientSecrets {ClientId = CLIENT_ID, ClientSecret = CLIENT_SECRET};
            try
            {
                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    clientSecrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(scriptFolder, true)).Result;
            }
            catch (Exception e)
            {
                Debug.LogError(e.ToString());
            }

            return credential;
        }

        private static bool MyRemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            var isOk = true;
            // If there are errors in the certificate chain, look at each error to determine the cause.
            // ReSharper disable once InvertIf
            if (sslPolicyErrors != SslPolicyErrors.None)
            {
                // ReSharper disable once ForCanBeConvertedToForeach
                for (var i = 0; i < chain.ChainStatus.Length; i++)
                {
                    // ReSharper disable once InvertIf
                    if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                    {
                        chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                        chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                        chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                        chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                        var chainIsValid = chain.Build((X509Certificate2) certificate);
                        // ReSharper disable once InvertIf
                        if (!chainIsValid)
                        {
                            Debug.LogError("certificate chain is not valid");
                            isOk = false;
                        }
                    }
                }
            }

            return isOk;
        }

        #endregion

        #endregion
    }
}