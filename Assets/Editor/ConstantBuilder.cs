﻿using UnityEngine;

namespace UniDb.Editor
{
    public static partial class ConstantBuilder
    {
        #region window editor

        #region -- db ------------------------------------------------

        public const string DATABSE_FULL_NAME = "master_db.asset";
        public const string DATABSE_NAME = "master_db";
        public const string DATABASE_PATH = "Root/Database";
        public const string NAME_MEMORY_DATDABASE_CLASS = "MemoryDatabase";

        #endregion

        #region -- template ------------------------------------------------

        public const string DATA_REPLACE_DATABASE_CREATOR = "\t\t\tbuilder.Append(new[]__bracket_open____data_replace____bracket_close__";
        public const string DATA_REPLACE_CLASS_NAME = "__class_name__";
        public const string DATA_REPLACE_PROPERTY_NAME = "__property_name__";
        public const string DATA_REPLACE_PROPERTY_VALUE = "__property_value__";
        public const string DATA_REPLACE_PROPERTY_INITIALIZE = "__property_name__ = __property_value__";
        public const string DATA_REPLACE = "__data_replace__";
        public const string DATA_REPLACE_BRACKET_OPEN = "__bracket_open__";
        public const string DATA_REPLACE_BRACKET_CLOSE = "__bracket_close__";
        public const string DATA_REPLACE_ONE_LINE_DATABASE_CREATOR = "new __class_name__ {__data_replace__},";
        public const string DATA_PROPERTY_DEFINE = "__property_define__";
        public const string DATA_VIEWER = "__data_viewer__";
        public const string DATA_REPLACE_PROPERTY_DEFINE = "private __class_name__[] __property_name__";
        public const string DATA_REPLACE_SWITCH_CASE = "\t\t\t\t\tcase \"__class_name__\":\n\t\t\t\t\t\t__data_replace__\n\t\t\t\t\t\tbreak;\n";

        #endregion

        #region -- other ------------------------------------------------

        public const string MENU_ITEM_CREATE_DATABASE_FILE = "Database/Create Database File";
        public const string MENU_ITEM_DATABASE_VIEWER = "Database/View Database";
        public const string MENU_ITEM_FETCH_ORIGIN_DATA = "Database/[Spreadsheet] Fetch Data";
        public const string MENU_ITEM_AIRTABLE_FETCH_ORIGIN_DATA = "Database/[Airtable] Fetch Data";

        public const int MENU_ITEM_START_PRIORITY = 300;
        public const string SAVE_SCRIPT_PATH_KEY = "scripts_path";

        public const string DBCREATOR_TEMPLATE_NAME = "DBCreatorTemplate";
        public const string DBVIEWER_TEMPLATE_NAME = "DBViewerTemplate";
        public const string INITIALIZER_TEMPLATE_NAME = "StartupTemplate";
        public const string SAVE_ASSEMBLY_KEY = "assembly_key";
        public const string ASSEMBLY_UNIDB_EDITOR_REFERENCE_KEY_START = "\"references\": [";
        public const string ASSEMBLY_UNIDB_EDITOR_REFERENCE_KEY_END = "],";

        #endregion

        #region -- color --------------------

        public const string GREEN_LOG_COLOR = "#25854B";
        public const int WIDTH_BUTTON = 150;

        #endregion------------------------------------------------------------\\

        #region -- airtable --------------------

        public const string SAVE_API_KEY = "airt_api_key";
        public const string SAVE_BASE_KEY = "air_base_key";
        public const string SAVE_LIST_TABLE_NAME = "air_table_name";

        #endregion------------------------------------------------------------\\

        #region -- google sheet --------------------

        public const string SAVE_SHEET_KEY = "ss_key";
        public const string SAVE_LIST_SHEET_NAME = "ss_sheet_name";

        #endregion------------------------------------------------------------\\

        #endregion
    }
}