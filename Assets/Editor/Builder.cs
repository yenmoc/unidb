﻿/*******************************************************
 * Copyright (C) 2020 worldreaver
 * __________________
 * All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * @author yenmoc phongsoyenmoc.diep@gmail.com
 *******************************************************/

#if UNITY_EDITOR
using System.IO;
using UniDb.Editor;
using UnityEditor;
using UnityEngine;
using UnityModule.EditorUtility;

public partial class Builder
{
    #region -- properties ------------------------------------------------

    private const string SCRIPT_PATH = "/Root/Scripts/";

    #endregion

    #region -- create db viewer ------------------------------------------------

    public static void CreateDatabaseViewer()
    {
        var type = TypeUtil.GetTypeByName(ConstantBuilder.NAME_MEMORY_DATDABASE_CLASS);
        if (type == null)
        {
            Debug.Log("Please execute generator MemoryMaster in setting menu [Database/Settings]");
            return;
        }

        var propertyInfos = type.GetProperties();
        if (propertyInfos.Length == 0)
        {
            Debug.Log("No tables exist in the database");
            return;
        }

        var databasePath = $"{Application.dataPath}/{ConstantBuilder.DATABASE_PATH}/{ConstantBuilder.DATABSE_FULL_NAME}";
        if (!File.Exists(databasePath))
        {
            Debug.Log($"Database file does not exist at the path: \n{databasePath}");
            return;
        }

        var propertyWrite = "";
        var propertyDataTransfer = "";
        var propertyViewer = "";

        for (int i = 0; i < propertyInfos.Length; i++)
        {
            var tableName = propertyInfos[i].Name;
            var className = tableName.Replace("Table", "");

            #region -- create properties ----------------------------------------

            var propertyDefine = ConstantBuilder.DATA_REPLACE_PROPERTY_DEFINE;
            var propertyName = $"_{className.ToLower()}s";
            propertyDefine = propertyDefine
                .Replace(ConstantBuilder.DATA_REPLACE_CLASS_NAME, className)
                .Replace(ConstantBuilder.DATA_REPLACE_PROPERTY_NAME, propertyName);
            propertyWrite += $"\t\t{propertyDefine};\n";

            #endregion

            #region -- pass value to properties ----------------------------------------

            var propertyTransfer = ConstantBuilder.DATA_REPLACE_SWITCH_CASE;
            var pData = $"{propertyName} = _db.{tableName}.All.ToArray();";
            propertyTransfer = propertyTransfer
                .Replace(ConstantBuilder.DATA_REPLACE_CLASS_NAME, className)
                .Replace(ConstantBuilder.DATA_REPLACE, pData);
            propertyDataTransfer += propertyTransfer;

            #endregion

            #region -- create viewer ----------------------------------------

            var pView = ConstantBuilder.DATA_REPLACE_SWITCH_CASE;
            var pViewData = "EditorGUILayout.BeginHorizontal();\n";

            pViewData += "\t\t\t\t\t\tfor (int j = 0; j < props.Length; j++)\n";
            pViewData += "\t\t\t\t\t\t{\n";
            pViewData += "\t\t\t\t\t\t\tEditorGUILayout.LabelField(props[j].Name, EditorStyles.linkLabel, GUILayout.Width(WIDTH_LAYOUT_MIN), GUILayout.MaxWidth(WIDTH_LAYOUT_MAX));\n";
            pViewData += "\t\t\t\t\t\t}\n";
            pViewData += "\n\t\t\t\t\t\tEditorGUILayout.EndHorizontal();\n";


            var typeClass = TypeUtil.GetTypeByName(className);
            if (typeClass == null) continue;
            var props = typeClass.GetProperties();

            pViewData += $"\t\t\t\t\t\tfor (int j = 0; j < {propertyName}.Length; j++)\n";
            pViewData += "\t\t\t\t\t\t{\n";
            pViewData += "\t\t\t\t\t\t\tEditorGUILayout.BeginHorizontal();\n";
            // ReSharper disable once ForCanBeConvertedToForeach
            for (int j = 0; j < props.Length; j++)
            {
                if (props[j].PropertyType.IsArray)
                {
                    pViewData += $"\t\t\t\t\t\t\tif (GUILayout.Button($\"View [{{{propertyName}[j].{props[j].Name}.Length}}]\", GUILayout.Width(WIDTH_LAYOUT_MIN), GUILayout.MaxWidth(WIDTH_LAYOUT_MAX)))\n";
                    pViewData += "\t\t\t\t\t\t\t{\n";
                    pViewData += $"\t\t\t\t\t\t\t\tvar propertyName = {propertyName}[j].{props[j].Name}.GetType().ToString();\n";
                    pViewData += $"\t\t\t\t\t\t\t\tvar view = CreateInstance<ViewArray>();\n";
                    pViewData += $"\t\t\t\t\t\t\t\tview.View(Array.ConvertAll({propertyName}[j].{props[j].Name}, Parse), $\"{{propertyName.Remove(propertyName.Length - 2)}} row [{{j}}]\");\n";
                    pViewData += "\t\t\t\t\t\t\t}\n";
                }
                else
                {
                    pViewData += $"\t\t\t\t\t\t\tEditorGUILayout.TextField({propertyName}[j].{props[j].Name}.ToString(), GUILayout.Width(WIDTH_LAYOUT_MIN), GUILayout.MaxWidth(WIDTH_LAYOUT_MAX));\n";
                }
            }

            pViewData += "\t\t\t\t\t\t\tEditorGUILayout.EndHorizontal();\n";
            pViewData += "\t\t\t\t\t\t}\n";

            pView = pView
                .Replace(ConstantBuilder.DATA_REPLACE_CLASS_NAME, className)
                .Replace(ConstantBuilder.DATA_REPLACE, pViewData);

            propertyViewer += pView;

            #endregion
        }

        var (data, _) = FunctionHelper.GetTemplateByName(ConstantBuilder.DBVIEWER_TEMPLATE_NAME);
        if (!PlayerPrefs.HasKey(ConstantBuilder.SAVE_SCRIPT_PATH_KEY))
        {
            EditorUtility.DisplayDialog("Missing Script Path", "You must select path folder scripts in menu setting via [Database/Settings]", "Ok");
            return;
        }

        var path = $"{Application.dataPath}/{PlayerPrefs.GetString(ConstantBuilder.SAVE_SCRIPT_PATH_KEY)}/Editor";
        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        data = data
            .Replace(ConstantBuilder.DATA_PROPERTY_DEFINE, propertyWrite)
            .Replace(ConstantBuilder.DATA_REPLACE, propertyDataTransfer)
            .Replace(ConstantBuilder.DATA_VIEWER, propertyViewer);

        FunctionHelper.WriteToFile($"{path}/DBViewer.cs".Replace("/", "\\"), data);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        Debug.Log($"<color={ConstantBuilder.GREEN_LOG_COLOR}>Create DBViewer complete!</color>");
    }

    #endregion

    #region -- create initializer ------------------------------------------------

    public static void CreateStartup()
    {
        var (data, _) = FunctionHelper.GetTemplateByName(ConstantBuilder.INITIALIZER_TEMPLATE_NAME);
        var dataInitializerPath = $"{Application.dataPath}/{SCRIPT_PATH}";
        if (!Directory.Exists(dataInitializerPath))
        {
            Directory.CreateDirectory(dataInitializerPath);
        }

        FunctionHelper.WriteToFile($"{dataInitializerPath}/Startup.cs".Replace("/", "\\"), data);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        Debug.Log($"<color={ConstantBuilder.GREEN_LOG_COLOR}>Create Startup complete!</color>");
        EditorUtil.AddDefineSymbols("UNIDB_INITIALIZED");
    }

    #endregion
}
#endif