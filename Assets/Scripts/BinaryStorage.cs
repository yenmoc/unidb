﻿using System;
using UnityEngine;

namespace Database
{
    [Serializable]
    public class BinaryStorage : ScriptableObject
    {
        public byte[] data;
    }
}