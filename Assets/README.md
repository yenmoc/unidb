# UniDb

## What
	- Database Helper fetch from google sheet or airtable to use in local by scriptable file

* SheetName == ClassName (TableName == ClassName)

Ex: Character Table

| Id | Name | Damage | Speed | Stamina |
|----|------|--------|-------|---------|
| 1	 | Hero |	10	 |	 4	 |    6	   |
| 2	 | Lumi |	8	 |	 5	 |    8	   |

* If the data of a field has a data type of string, it must be enclosed in "".
Ex: value of field Name : "Forgotten Hero"

| Id | Name | Damage | Speed | Stamina |
|----|------|--------|-------|---------|
| 1	 |"Hero"|	10	 |	 4	 |    6	   |
| 2	 |"Lumi"|	8	 |	 5	 |    8	   |

* If the data of field has a multipe value it must be split with ','

Ex:

| Id  | 	  MaxLevel 		|
|-----|---------------------|
| 1   |   5,10,15,20,25,30	|
| 2   |   5				    |
| 3   |   5,10,15,20		|

* If the data of a field has both int and float data types, then if the data is in float type, it needs to add f in the back.

Ex:

| Id  | BaseValue |
|-----|-----------|
| 1   |   10      |
| 2   |   5.5f    |
| 3   |   0.5f    |

## Installation

```bash
"com.yenmoc.unidb":"https://gitlab.com/yenmoc/unidb"
or
npm publish --registry http://localhost:4873
```

## Requirement

* [MessagePack-CSharp](https://github.com/neuecc/MessagePack-CSharp)
* [MasterMemory](https://github.com/Cysharp/MasterMemory)

## Usage

* The First you need run generate startup file in menu setting via the `Database/Settings`.
  this will help create class Startup.cs, this class help register resolver

* The Second you need run memory master generate in menu setting via the menu item `Database/Settings`

* The Third you need run message pack generate in menu setting via the menu item `Database/Settings`

* You can fetch data from spreadsheet via the menu item `Database/[Spreadsheet] Fetch Data`

* You can fetch data from airtable via the menu item `Database/[Airtable] Fetch Data`

* After fetch data you need create database file via the menu item `Database/Create Database File` 